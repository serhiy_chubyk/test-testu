package pl.lamasoft;

        import org.junit.jupiter.api.Assertions;
        import org.junit.jupiter.api.BeforeEach;
        import org.junit.jupiter.api.Test;

public class CalculatorTest {

    Calculator testowyCalculator = new Calculator();
    final float testowyRabatDodatni = 1.008f;     //ustalamy, że rabat dodatni to ZWYŻKA
    final float testowyRabatUjemny = -0.998f;     //ustalamy, że rabat ujemny to ZNIŻKA
    float testowaWartoscCalkowitaZRabatemZerowym = testowyCalculator.getIlosc()* testowyCalculator.getCenaJednostkowa();

    @BeforeEach
    public void setUp(){
        //jak na razie obiekt testowyCalculator nie ma ustalonych zadnych pól
        //teraz przed każdym z tych testów na dole ustawiamy dla tego obiektu dwa pola
        testowyCalculator.setCenaJednostkowa(1.2312f);
        testowyCalculator.setIlosc(21412412);

    }

    @Test
    public void bardzoOpisowaNazwaTestuKtoraMaFchujDoOpisania(){

    }


    @Test
    public void MetodaLiczWartoscCalegoZamowieniaPrzyDodatnimRabaciePowinnaZwrocicWiekszaWartoscCalkowitaNizZRabatemZerowym(){
        // tu bierzemy obiekt testowyCalculator z dwoma ustalonymi polami w metodzie setUp
        // i ustawiamy mu kolejne, trzecie pole. jest nim pole rabat. ustawiamy mu wartosc testowyRabatDodatni

        testowyCalculator.setRabat(testowyRabatDodatni);


        Assertions.assertTrue(testowyCalculator.liczWartoscCalegoZamowienia()>testowaWartoscCalkowitaZRabatemZerowym);
    }

    //TODO 1 napisac analogiczną metodę do powyższej (nie korzystając z metody Copy'ego Paste'a) i nie patrząc na nią - tylko z głowy dla ujemnego rabatu


    //TODO 2 wymyśleć własny scenariusz co może pójść nie tak i go zrealizować



}
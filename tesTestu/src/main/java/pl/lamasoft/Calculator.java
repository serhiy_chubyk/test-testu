package pl.lamasoft;

public class Calculator {

    private float cenaJednostkowa;
    private float rabat;  //ustalamy że rabat dodatni to ZWYŻKA, a ujemny to ZNIŻKA
    private long ilosc;
    float wartoscCalegoZamowienia;

    public Calculator() {
    }

    public float getCenaJednostkowa() {
        return cenaJednostkowa;
    }

    public void setCenaJednostkowa(float cenaJednostkowa) {
        this.cenaJednostkowa = cenaJednostkowa;
    }

    public float getRabat() {
        return rabat;
    }

    public void setRabat(float rabat) {
        this.rabat = rabat;
    }

    public long getIlosc() {
        return ilosc;
    }

    public void setIlosc(long ilosc) {
        this.ilosc = ilosc;
    }

    public float getWartoscCalegoZamowienia() {
        return wartoscCalegoZamowienia;
    }

    // standardowy setter jest troche bez sensu skoro robimy kalkulator
    // nie ma sensu zeby ustawiac pole wartoscCalegoZamowienia np na wartosc -100 jezeli rabat=10, cenaJednostkowa=1, a ilosc=100
    // dlatego albo skasujemy ta metodę, albo zmienimy ją tak, aby niczego nie przyjmowala i wywoływała metodę liczWartoscCalegoZamowienia ustawiajac pole wattoscCalegoZamowiania na to co zwraca metoda liczaca
    public void setWartoscCalegoZamowienia(float wartoscCalegoZamowienia) {
        this.wartoscCalegoZamowienia = wartoscCalegoZamowienia;
    }


    public float liczWartoscCalegoZamowienia() {
        return cenaJednostkowa*ilosc*rabat;
    }
}
